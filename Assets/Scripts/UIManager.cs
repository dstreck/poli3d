﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public TextMesh poliedroText;

    public Text arestasText;
    public Text verticesText;
    public Text facesText;

    void Awake()
    {
        if (instance == null)
            // if not, set instance to this
            instance = this;

        // if instance already exists and it's not this:
        else if (instance != this)
            // then destroy this, meaning there can only ever be one instance of a HUDManager.
            Destroy(gameObject);
    }
    
}
