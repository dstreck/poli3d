﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public PoliedroScript poliedro;

    private MenuManager _menuManager;

    public PoliedroScript[] poliedros;

    void Awake()
    {
        // check if instance already exists
        if (instance == null)
            // if not, set instance to this
            instance = this;

        // if instance already exists and it's not this:
        else if (instance != this)
            // then destroy this, meaning there can only ever be one instance of a HUDManager.
            Destroy(gameObject);

        GameObject menu = GameObject.Find("MenuManager");

        if (menu)
        {
            _menuManager = menu.GetComponent<MenuManager>();
            for (int i = 0; i < poliedros.Length; i++)
            {
                poliedros[i].gameObject.SetActive(false);
                if (poliedros[i].poliedro == _menuManager.poliedroSelecionado)
                {
                    poliedros[i].gameObject.SetActive(true);
                    poliedro = poliedros[i];

                    UIManager.instance.poliedroText.text = poliedros[i].poliedro.ToString();

                    UIManager.instance.arestasText.text = poliedros[i].numArestas.ToString();
                    UIManager.instance.verticesText.text = poliedros[i].numVertices.ToString();
                    UIManager.instance.facesText.text = poliedros[i].numFaces.ToString();
                }
            }
        }
        else
        {
            poliedros[0].gameObject.SetActive(true);
            poliedro = poliedros[0];

            UIManager.instance.poliedroText.text = poliedros[0].poliedro.ToString();

            UIManager.instance.arestasText.text = poliedros[0].numArestas.ToString();
            UIManager.instance.verticesText.text = poliedros[0].numVertices.ToString();
            UIManager.instance.facesText.text = poliedros[0].numFaces.ToString();
        }
    }

    public void AtualizarPlanificacao(float val)
    {
        poliedro.MontarTudo(val);
    }
}
